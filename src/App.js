import React, { useState, useEffect, useRef } from "react";
import { init } from "pell";
import "pell/dist/pell.css";

function App() {
  const [html, setHTML] = useState("");
  const editor = useRef(null);

  useEffect(() => {
    setTimeout(() => {
      editor.current.content.innerHTML = "<p>Queried data</p>";
    }, 0);
  }, []);

  useEffect(() => {
    editor.current = init({
      element: document.getElementById("editor"),
      onChange: (html) => setHTML(html),
      defaultParagraphSeparator: "p",
      actions: ['bold', 'underline', 'italic'],
    });
  }, []);

  return (
    <div>
      <h3>Editor:</h3>
      <div ref={editor} id="editor" className="pell" />
      <h3>HTML Output:</h3>
      <div>{html}</div>
    </div>
  );
}

export default App;
