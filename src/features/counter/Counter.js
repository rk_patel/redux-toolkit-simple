import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { addTodo, deleteTodo } from './counterSlice'

export function Counter() {
    const list = useSelector((state) => state.counter.value)
    const [todoDescription, setTodoDescription] = useState("");
    const dispatch = useDispatch()

    return (
        <div>
            <div>
                <div>
                    <input onChange={(e) => setTodoDescription(e.target.value)}
                        value={todoDescription} />
                </div>
                <button
                    aria-label="Increment value"
                    onClick={() => dispatch(addTodo(todoDescription))}
                >
                    Add
                </button>
            </div>
            <div>
                {list?.map((todo) => (
                    <li>{todo}
                        <button
                            aria-label="Decrement value"
                            onClick={() => dispatch(deleteTodo())}
                        >
                            Delete
                        </button>
                    </li>
                ))}
            </div>
        </div>
    )
}