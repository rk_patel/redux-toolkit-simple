import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    value: [],
}

export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        addTodo: (state, action) => {
            // state.value += 1
            state.value.push(action.payload)
        },
        deleteTodo: (state, action) => {
            debugger
            // state.value -= 1
            const index = state.value.findIndex((todo) => todo === action.payload);
            state.value.splice(index, 1);
        },
    },
})

export const { addTodo, deleteTodo } = counterSlice.actions

export default counterSlice.reducer